# Farm Management App Backend
https://github.com/quickaccount/PlanHarvest-backend

This project was used primarily to learn about how to design, manipulate, and impliment databases. It is not intended to be deployed in a production environment.

The project is divided into functions that allow a farmer to manage assets (fields, grain-bins, hay-sheds), products (various grains, hay bales, straw bales), and contracts for products with customers.

It was deployed locally for a demonstration.


## Back-end part of a group project for a database course.

    Basics of running PosgreSQL on Linux
    Relational database management
    Protection against sql injection
    Api built using ExpressJS
    Project management

